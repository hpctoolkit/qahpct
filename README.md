<!--
SPDX-FileCopyrightText: Contributors to the HPCToolkit Project

SPDX-License-Identifier: CC-BY-4.0
-->

# `QAHPCT`

**Introduction**

`QAHPCT` is an infrastructure to run arbitrary sets of arbitrary
tests on arbitrary executables in arbitrary locations in the file system.
The infrastructure is based on that used with `minitest`, which
has now been incorporated into `QAHPCT`.

It is designed to support a retail mode to run one single test on a
single exexutable, and it supports a wholesale mode to run many
tests on many executables.  Both of these depend on tests described with
a Data Descriptor, as detailed in the file `DD_README.md`.

For either usage, the user must first customize the `QAHPCTmodule`
file to point to the installation, copy it to wherever
modules are loaded from, and then load it to set up the
paths needed for testing.

The next section describes the retail mode of operation.
The following section described the wholesale mode of operation,
for both a single QA-Test-Directory, or a list of them.
The section after that covers the repository structure in more detail.
The following section describes the structure of a QA-Test-Directory.
The section after that describes the specification of individual
tests, followed by a section describing
handling of possible user errors.
Finally, at the end of this README.md file, there are instructions
for cloning QAHPCT and running tests.

**Retail Mode**

The retail mode of using `QAHPCT` uses one command, `runone`.
It is invoked to run one test, from any directory, as:

	runone [options] <Data-descriptor> <target> "<target-arguments>"


`<Data-descriptor>` is written as described detailed in the file `DD_README.md`.

`<target>` is an executable that can be either in the current
directory, or elsewhere.

`<target-arguments>` is a quoted string to allow for blanks
in the arguments.  If no arguments are needed, it should be written
as `""`.

If the `<Data-descriptor>` begins with `run.`, the target will be run
with its arguments, but without any data-collection or processing.
It will record a file named `log.<Data-descriptor>` in the current
directory.

If the `<Data-descriptor>` begins with `expt.`, the target will be run
using `hpcrun` to collect data, which will be recorded in a directory,
`meas.<Data-descriptor>` in the current directory.
The test will then run `hpcstruct` on that directory, followed by
`hpcprof`, resulting in a `dbase.<Data-descriptor>` directory
in the current directory.  All three phases of the test will be checked
for errors, and a file named `log.<Data-descriptor>`
in the current directory, will contain the results from all three
phases of the test.

_Options to `runone`_

"`-x`", means do not run the test, but just prints the command line.

"`-timeout=<value>`" sets a timeout of `<value>` for each test;
if `<value>` is zero, no timeout is set.
If omitted, a default timeout 60 seconds will be applied.

**Wholesale Mode -- Single QA-Test-Directory**

`QAHPCT` works on one or more QA-Test-Directory’s.
Each such directory contains an executable on which tests are to be run.

Wholesale mode can be used in two ways, either to look at a single
QA-Test-Directory, or to scan a list of them.  Each QA-Test-Directory’s
`QA.<dirname>` script defines the target and its
arguments, finds the appropriate `QATestList.<suitemode>.<dirname>`,
and sequentially performs each test in that list.

To look at a single QA-Test-Directory, say one named `/a/b/c/whatever`,
first ensure that there is a
`QA.whatever` script written, and located within the `qascripts`
sub-directory in the `QAHPCT` repository.
Also ensure that there is a `QATestList.<suitemode>.whatever`
file containing the data-descriptors of the desired tests,
for each `<suite-mode>` desired in the `qatestlists` subdirectory in
the `QAHPCT` repository.
Then simply go to the directory and invoke “`QA.whatever <suite-mode>`”.
The tests as described for that `<suite-mode>` will be run,
and the results reported on the terminal and in a log file.

**Wholesale Mode -- Multiple QA-Test-Directories**

To process a list of QA-Test-Directory’s, go to the `QAHPCT_ROOT`
directory (defined in the module), and edit the `qadirlists/<machine>`
file to verify the list of QA-Test-Directory’s to be sampled.
Each such directory should have the appropriate script and
QATestList file.
Then, invoke the script as `qahpct <suite-mode>`.

**The Top-level `QAHPCT` Directory**

The top-level `QAHPCT/bin` directory contains three scripts,
`qahpct`, and `runone`, and `qasum`.
It also has a `qadirlists` directory containing per-machine
configuration files.
Each configuration file is a list of QA-Test-Directory’s.
Those directories are structured as described below.
(Note that JMC would prefer a more complex tree structure
to traverse the QA-Test-Directory’s.
That can be added later.)

The repository also contains a `QAHPCTmodule` file.
That file **must** be customized to point to wherever the
`QAHPCT` repository was cloned,
and copied to wherever modules may be loaded.

The `QAHPCTmodule` exports `QAHPCT_ROOT` pointing to the topmost
directoryof the repository.
It also augments the user’s path with the addition of the
`QAHPCT/bin` and `QAHPCT/qascripts` directories.
With that module loaded, any of the QA.`<dirname>` scripts can be run by
cd’ing to the QA-Test-Directory
and directly invoking it.

The `QAHPCTmodule`  also exports the definitions of `MINITEST_ROOT`
and augments the
user's path with the `QAHPCT/minitest/bin` directory for the use
with the `minitest`
subdirectory of `QAHPCT`.

The `QAHPCT` repository contains the QAscript’s,
in a subdirectory, `qascripts`; zero or more QATestList’s in a
subdirectory, `qatestlists`.
It also includes a directory, `qadirlsts`, containing files named by
the machine for which they are intended.

The `qahpct` script accepts a single argument `suite-type`,
describing the type of run desired.
The string is architecturally completely arbitrary.
Many QA-Test-Directory’s will support only some `suite-type` arguments.
The `qahpct` script reads that machine's `qadirlists/<machine>` file,
and cycles through the QA-Test-Directory’s named therein.
It goes to each QA-Test-Directory, `<dirname>`, and invokes
the `qascripts/QA.<dirname>` script corresponding to it,
passing in the `suite-type` argument. Each `qahpct` script execution
creates a log file of its operations, `log.qahpct.<suite-type>`.

The `QAHPCT` directory also has a sub-directory named `qaexamples` that
contains a few simple example QA-Test-Directory's.

One is named `generic`.
It has the source and Makefile for building a trivial application,
also named `generic`.
The `QAHPCT` repository has the corresponding `qascripts/QA.generic`
and `qatestlists/QATestList.smoke.generic` files.

Others are named `mpi_example`, `pytest`, and `cg.pytest.cuda`,
each representing another simple test case.
The repository has the appropriate `qascripts/QA.<name>`
and `qatestlists/QATestList.smoke.<name` files for each of them.

The `QAHPCT` directory also has a subdirectory `minitest` containing the
pre-existing `minitest` test suite.  See its README.md file.

**QA-Test-Directory Structure**

A QA-Test-Directory is defined by a `<pathname>`.
The term `<dirname>` used below is the basename of the `<pathname>`.
Initially, it will be assumed that the executable to be tested
(the target) exists, already built,
somewhere in the QA-Test-Directory’s file-system tree.

Each QA-Test-Directory, `<dirname>`, has a matching script file, named
`QA.<dirname>`, in the `QAHPCT` repository subdirectory
named `qascripts`.
(Note that this mandates that no two directories have the same
basename, unless they use exactly the same `QA.<dirname>` script.)

The script file is invoked with a single parameter, `suite-type`.
The `QA.<dirname>` script has complete control over which
`suite-type`’s are recognized and supported for the
tests in that QA-Test-Directory, and how that parameter translates
into the set of tests to be run.

The `QA.<dirname>` script is responsible for exporting shell variables
with the target name and target arguments to be used for the run.
If needed, the script may also export a timeout parameter.
It is also responsible, if appropriate, for the loading and unloading
of the modules needed to run the target.
After exporting the shell variables, all of the scripts invoke the
same subscript to do the actual processing.

For most QA-Test-Directory’s the set of tests to be run is given
by a file, named `QATestList.<suite-type>.<dirname>`,
in the `QAHPCT` repository subdirectory named `qatestlists`.
If that file does not exist, it means that the QA-Test-Directory
does not support that `suite-type`,
and the script will consider all tests to pass.
Each entry in the file is formatted as described in 
DD_README.md page.

The QA-Test-Directory `minitest` manages its test lists
internally, rather than use a `QATestList` file
contained in the `QAHPCT` repository; it makes its own
decisions about which `suite-type`’s are supported.

[Not Yet Implemented]
For some QA-Test-Directory’s there is a matching `QAModuleList` file,
named `QAModuleList.<dirname>`,
in the `QAHPCT` repository subdirectory, `qamodules`.
If that file exists, it lists the system modules
that must be loaded for the target executable to run.
Those modules should be loaded by the QAscript file, and should be
unloaded when the script terminates.
Loading and unloading of modules is done by scripts in the
`/bin` directory`, named `QAModLoad` and `QAModUnload`.
Each can be invoked multiple times within a script file, and the
script file should leave the modules
loaded as they were before it was invoked.
 
Each invocation of a `QA.<dirname>` script, whether independently or
by virtue of invoking `qahpct`, will create a log file of all
operations in that directory, named `LOG.<dirname>.<suite-type>`.


**Instructions for Cloning `QAHPCT` and Running Tests**

The `QAHPCT` repository is in gitlab.  It can be cloned with

	git clone https://gitlab.com/hpctoolkit/qahpct.git
	     or
	git clone git@gitlab.com:hpctoolkit/qahpct.git (Using SSH)

Go to wherever you have cloned the repository, and edit the file
named `QAHPCTmodule` to set `QAHPCT_ROOT` to that location, and
update the other commands in that file to match.

Copy that file to wherever you load modules from and load
that module.

Edit the file `qadirlists/<machine>` to list the directories you want
to test with a single command on the named machine.

Make sure that each such directory has the necessary `QA.<dirmame>`
script in directory `qascripts` subdirectory and the list
of desired tests are in `QATestList.<suitemode>.<dirname>`
in directory`qatestlists`.

You can now run the`qahpct` command for a run of many directories,
or you can `cd` to `<dirname>` and directly run `QA.<dirname>` with
the `<suitemode>` you want.
