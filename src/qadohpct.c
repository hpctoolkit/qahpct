/*
 * SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/*
 * qadohpct -- invoked with two arguments
 *	arg1 "target"
 *	arg2 "target-args"
 *	    Alternatively the minitest target arguments can be specified by tags .MN,nnn, .MK,nnn, and ,MI.nnn
 *	arg3 expt.*
 *	    or
 *	arg3 run.*
 *		where the * represents a name for the logfile, as described in the README.md file
 */

#include  <stdlib.h>
#include  <stdio.h>
#include  <errno.h>
#include  <unistd.h>
#include  <string.h>
#include  <ctype.h>

/* routine for error exit; ensure log file is created */
void  err_exit(char *);
void  err_clear();

/* routine to print a help message */
void  print_help(void);

/* routine to process possible command line options */
void  dotimer( char *value);		/* specify that the run have a timeout of value */

/* routine to process the experiment descriptor */
void processdescriptor(char *cmdline);

/* various items in the experiment descriptor */
void  dothreadnumber(char *, char *);   /* Number of threads specified; modifier must be zero */
void  cputime(char *, char *);    /* specify -e CPUTIME; modifier is either h or l */
void  cycles(char *, char *);     /* specify -e cycles; modifier is either h or l */
void  papicycles(char *, char *); /* specify -e PAPI_TOT_CYCLES; modifier is either h or l */
void  realtime(char *, char *);   /* specify -e REALTIME; modifier is either h or l */
void  insts(char *, char *);      /* specify -e instructions; modifier is either h or l */

void  lvl0(char *, char *);       /* specify -e gpu=level0; may be either ",pc", or ",k=..." */

void  cuda(char *, char *);       /* specify -e gpu=cuda; modifier may be ",pc" */

void  rocm(char *, char *);       /* specify -e gpu=rocm; modifier may be ",pc" */
void  rocmcounter(char *, char *);/* specify -e rocm::* modifier must be zero */

void  debugon(char *, char *);    /* specify -d to enable attaching a debugger; modifier must be zero */
void  dbldebugon(char *, char *); /* specify -dd modifier to enable debugging runtime subsystem;  modifier must not be zero */
void  traceon(char *, char *);    /* specify -t to enable tracing; modifier must be zero */
void  tracker(char *, char *);    /* specify simulating LLNL's tracker; modifier must be zero */
void  mpitracker(char *, char *); /* specify simulating LLNL's tracker by a different name,modifier must be zero */
void  argoverride(char *, char *);    /* provide a user argument to replace the one passed in on the command */
void  python(char *, char *);     /* specify -a python; modifier much be zero */

void  minitestN(char *, char *);  /* pass the -N argument to the minitest target */
void  minitestI(char *, char *);  /* pass the -I argument to the minitest target */
void  minitestK(char *, char *);  /* pass the -K argument to the minitest target */

void  mpisetup(char *, char *);   /* specify an MPI run; the modifier gives the number of ranks */
void  runsetup(char *, char *);   /* specify an execute-only run -- no hpcrun arguments allowed */

void parse_number (char *p);      /* ensure argument is a valid number */
void process_profile_modifier(char *verb, char* mod, bool is_event);	/* process profile frequency modifier */
void process_lvl0_modifier(char *verb, char* mod);	/* process lvl0 modifier */

void userhpcarg( char*verb, char *mod);  /* process an unknown string into -e string for hpcrun */

/* lookup table for command-line options */
#if 0
struct  cmdtab {
  const char  *name;
  void (*function)(void);
};
struct cmdtab cmdtab[] = {
  {"-nt", dotimer },
  {"-x" , donotrun }
#endif

/* lookup table for behavior scripts */
struct  scripttab {
  const char  *name;
  void (*function)(char *, char *);
};

// "rocm::*" special-case'd first
struct  scripttab  scripttab[] = {
  {"cputime", cputime },
  {"cycles", cycles },
  {"papicycles", papicycles },
  {"realtime", realtime },
  {"insts", insts },

  {"lvl0", lvl0 },

  {"cuda", cuda },

  {"rocm", rocm },

  {"d", debugon },
  {"dd", dbldebugon },
  {"t", traceon },
  {"tracker", tracker },
  {"mpitracker", mpitracker },
  {"arg", argoverride },
  {"python", python },
  {"MN", minitestN },
  {"MI", minitestI },
  {"MK", minitestK },
  {"mpi", mpisetup },
  {NULL, NULL}
};

char  prepends[1024] = "";
char  targname[1024] = "";
char  targargs[1024] = "";
char  usrtargargs[1024] = "";
char  runargs[1024] = "";
char  hpcstructargs[1024] = "";
char  cmdbuf[4096] = "";
bool  mpimode = false;
char* mpiranks;
char* thread_cnt = NULL;
bool  runmode = false;
char  err_buf[1024];
char *ddesc;
bool  norun = false;

int
main(int argc, char **argv)
{
  // ensure there's no left-over error file
  err_clear();

#if 0
  /*  Log the invocation */
  {
    char *pp;
    fprintf(stderr, "qadohpct %d arguments:", argc);
    pp  = *argv;
    for (int ii = 0; ii < argc; ii ++) {
      fprintf(stderr, "  argv[%d] = %s ", ii,  argv[ii]);
      pp ++; 
    }   
    fprintf(stderr, "\n");
  }
#endif
  /* look for any "-" arguments */
  char    **p;
  int i;
  p = &argv[1];
  if (*p == NULL) {
    print_help();
    exit(0);
  }
  int nargc = argc-1;
  int argskip = 0;
  for (i = 1; i < argc; i ++) {
    if ( *p[0] == '-' ) {
          // fprintf(stderr, " cmdline arg[%d] = %s   nargc = %d\n", i, *p, nargc) ;

      if ( strncmp ("-timeout" , *p, strlen("-timeout") ) == 0 ) {
	// found -timeout, see if it is followed by = sign
	char *argstring = *p;
	int len = strlen (argstring);
	int cutoff = strlen("-timeout");
          // fprintf(stderr, " Processing  %s ,length %d, cutoff %d \n", *p, len, cutoff) ;
          // fprintf(stderr, "     argstring[cutoff] = %c ,argstring[cutoff+1] = %c \n", argstring[cutoff], argstring[cutoff+1]) ;

	char linker =  argstring[cutoff] ; 
        if (  linker != '=' ) {
	  sprintf(err_buf, " ERROR: Unrecognized timeout parameter character \"%c\" (should be \"=\" ) in \"%s\"\n", linker, *p );
          err_exit (err_buf);
        }
        char *tmp = & argstring[cutoff+1]  ;
        dotimer( tmp );

      } else if (strcmp ("-x" , *p ) == 0 ) {
        norun = true;

      } else if (strcmp ("-h" , *p ) == 0 ) {
        print_help();
	exit(0);

      } else {
        /* unrecognized argument */
	sprintf(err_buf, " ERROR: Unrecognized cmdline argument \"%s\"\n", *p );
        err_exit (err_buf);
      }

      /* now continue to process next argument */
      p++;
      nargc --;
      continue;
    } else {
      // End of cmdline arguments, continue to real args
      break;
    }
  }   
  // fprintf(stderr, "  argument[%d] = \"%s\"\n", nargc, *p );
  if (nargc != 3) {
    ddesc = strdup("bad_args");
    sprintf(err_buf, "ERROR: qadohpct must be invoked with three non-\"-\" arguments, not %d\n", nargc );
    err_exit (err_buf);
  }
    
#if 1
  // The next argument is the target, save it
  if ( *p[0] == '/' ) {
    strcpy (targname, *p);
  } else {
    strcpy (targname, "./");
    strcat (targname, *p);
  }
#else
  strcpy (targname, *p);
#endif
  p++;
  nargc --;
  // The following argument is the target arguments, save it
  strcpy (targargs, *p);
  p++;
  nargc --;

  // The next argument is the data decsriptor
  ddesc = *(p);
  nargc --;

  /* verify the remaining argument count is zero */
  if (nargc != 0) {
    ddesc = strdup("bad_args");
    sprintf(err_buf, "ERROR: qadohpct must be invoked with three non-\"-\" arguments, not %d", nargc+3 );
    err_exit (err_buf);
  }

  // fprintf (stderr, "  --> targname = \"%s\"; targargs = \"%s\"; ddesc = \"%s\"\n", targname, targargs, ddesc);

  /* Process the descriptor */
  processdescriptor(ddesc);

  // ensure thread count set
  if (thread_cnt == NULL ) {
    strcat (prepends, " export OMP_NUM_THREADS=1;" );
    thread_cnt = strdup("1");
  }

  // see if user set argument override
  // fprintf (stderr, "Before: targargs = \"%s\";  usrtargargs = \"%s\"\n", targargs, usrtargargs);
  if ( strlen (usrtargargs) != 0 ) {
    strcpy (targargs, usrtargargs);
  }
  // fprintf (stderr, " After:targargs = \"%s\";  usrtargargs = \"%s\"\n", targargs, usrtargargs);

  /* format up the command to be invoked via system() */
  if (runmode == true ) {
    // if we are just running the target, check for invalid arguments
    if ( strlen (runargs) != 0 ) {
      sprintf(err_buf, "%s can not be run -- hpcrun arguments may not be supplied", argv[2]);
      err_exit (err_buf);
    }
    if ( strlen (hpcstructargs) != 0 ) {
      sprintf(err_buf, "%s can not be run -- hpcstruct arguments may not be implied", argv[2]);
      err_exit (err_buf);
    }

    // Run either runqampirun or runrun with the appropriate arguments
    if (mpimode == true) {
      // format the runqampirun command
      sprintf(cmdbuf, " %s runqampirun \"-np %s\" \"%s\" \"%s %s\" %s ",
        		prepends, mpiranks, prepends, targname, targargs, ddesc );
    } else {
      // format the runrun command
      sprintf(cmdbuf, "%s runqarun \"%s %s\" %s",
        prepends, targname, targargs, ddesc );
    }
 
  } else {
     // runmode is false -- we're running an experiment
     // Run either runqampihpct or runqahpct with the appropriate arguments
     if (mpimode == true) {
      // format the runqampihpct command
      sprintf(cmdbuf, " %s runqampihpct \"-np %s\"  \"%s\" \"%s %s\"  \"%s\"     %s       \"%s\" ",
	       		prepends, mpiranks, prepends, targname, targargs, runargs, ddesc, hpcstructargs );
    } else {
      // format the runqahpct command
      sprintf(cmdbuf, "%s runqahpct \"%s %s\" \"%s\" %s \"%s\" ",
        prepends, targname, targargs, runargs, ddesc, hpcstructargs );
    }
  }
  // fprintf(stderr, "  --> command to be run =\"%s\"\n", cmdbuf);

  if (norun == false ) {
    /* now actually call system to run the command */
    system(cmdbuf);
  } else {
    // Don't do it
    fprintf(stderr, "  --> command (not being run)  =\"%s\"\n", cmdbuf);
  }
  return 0;
}

/* processdescriptor -- process an experiment descriptor string:
 *  items are separated by a . character; each item is looked-up
 *  in a table, giving the routine to process it; the routine is then called
 */
void
processdescriptor(char *cmdline)
{
  char  *p;
  char  *j;
  char  prevj;
  struct  scripttab  *k;
  char  buf[1024];
  int stindex;

  /* Log the event */
  // fprintf( stderr, " Begin processdescriptor(%s)\n", cmdline);

  /* ensure that the descriptor begins with "run." or "expt." */
  stindex = 5;
  if (strncmp ("run.", cmdline, 4) == 0 ) {
    runmode = true;
    stindex = 4;

  } else if (strncmp ("expt.", cmdline, 5) != 0 ) {
    sprintf  (err_buf, "Test descriptor must begin \"run.\" or \"expt.\"; \"%s\" does not", cmdline);
    err_exit (err_buf);
  }

  char *copy = strdup(&cmdline[stindex]);
  // fprintf( stderr, " copied  processdescriptor(%s)\n", copy);

  p = copy;
  while( (*p) != 0 ){
    /* First check for a user-specified string */
    if ( *p == '-' ) {
      // fprintf (stderr, "User comment \"%s\"\n", p );
      /* nothing needed to do with that string */
      /* stop processing -- User comment must be last */
      break;
    }

    /* find the terminator for this item (a . or NULL) */
    j = p;
    while( ((*j) != 0) && ((*j) != '.') ) {
      j++;
    }

    // either a . end of item) or NULL (end of the string)
    prevj = *j;		// the terminator
    *j = 0;		// terminate the copy of the string at that item

    // set pointer to the next potential item
    char * nextp = j;
    if (prevj != 0 ) {
      nextp ++;
    }

    // store this verb pointer
    char *verb = p;
    char *modifier = NULL;
    // fprintf( stderr, " processdescriptor item: %s\n", verb);

    // Look for a possible modifier
    while( ((*p) != 0) && ((*p) != ',') ) {
      p++;
    }
    if ( (*p) == ',') {
      // there's a modifier
      *p = 0;		// terminate the verb
      p++;		// move past the comma
      modifier = p;	// set the modifier pointer
      // fprintf( stderr, " verb= %s,  modifier past comma: %s\n", verb, modifier);
      // See if the modifier is a %-quoted string
      if (*p == '@') {
        // the item terminator may be a '.' in the middle of the quoted string.
        // restore the previous item terminator character back
        *j = prevj;

        // skip the opening @-quote
        modifier ++;
        // fprintf( stderr, " modifier skipping initial @-quote: %s\n", modifier);

        // now find the closing quote
        p ++;
        while  ( (*(p) != 0) && (*(p) != '@') ) {
          p++;
	}
	// p is either pointing to the closing quote, or to the end of the stringo
        // fprintf( stderr, " quoted string terminator: \"%s\"\n", p);
        if (*(p) == 0 ) {
          // there was no closing quote -- an error
          sprintf (err_buf, "quoted string modifier is not terminated");
          err_exit (err_buf);
        }
        *p = 0;
        // move to chanacter after the close quote, the real terminator
        // it should be either EOL or a '.'
	p++;
	prevj = *p;
        *p = 0;
        nextp = p++;
        if (prevj != 0) {
          nextp++;
        }
      }
    }
    // fprintf( stderr, "   item: %s, modifier: %s\n", verb, (modifier == NULL? "NULL" : modifier) );

    // Look for generic argument of rocm::
    if(strncmp(verb, "rocm::", 6) == 0) {
      //Alas, we can't use the real counter names
      sprintf (err_buf, "rocm counters must be named by \"rocm__\", not \"rocm::\";  sorry.");
      err_exit ( err_buf);
    }

    // and now look for the supported generic argument of rocm__
    if(strncmp(verb, "rocm__", 6) == 0) {
      rocmcounter(verb, modifier);

      /* continue processing */
      p = nextp;
      continue;
    }

    /* initialize table pointer, to cope with finding a number or a -user-string */
    k = &scripttab[0];

    /* check for a numeric argument */
    if ( isdigit( (int)verb[0] ) != 0 ) {
      dothreadnumber(verb, modifier);
      /* continue processing */
      p = nextp;
      continue;
    }

    /* now look up the item in the table */
    for (k = &scripttab[0]; ; k++) {
      /* end-of table -- no match to be found */
      if(k->name == NULL) {
        break;
      }

      if(strcmp(verb, k->name) == 0) {
        /* found a match, process it */
        // fprintf(stderr, "Match %s found\n", verb);
        (k->function)(verb, modifier);
        break;
      }
    }

    if(k->name == NULL) {
      /* break was at end of table */
      sprintf(err_buf, "Verb `%s' not recognized", verb);
      userhpcarg (verb, modifier);
    }

    /* continue processing */
    p = nextp;
  }
  // reached the end of the input string
}

void
parse_number (char *p) 
{
  /* make sure the string is really an integer */
  for (int i = 0; ; i++ ) {
    if ( p[i] == 0 ) {
      break;
    }
    if ( isdigit( (int)p[i] ) == 0 ) {
      sprintf( err_buf, "Number string %s contains a non-digit value", p);
      err_exit ( err_buf);
    }
  }
}

void
dothreadnumber(char *p, char *mod)
{
  if (mod != NULL ) {
    sprintf( err_buf, "thread-number string %s may not have a modifier (%s)", p, mod);
    err_exit(err_buf);
  }

  parse_number (p);
  thread_cnt = p;

  /* Put together the directive, and add to prepends */
  strcat (prepends, "export OMP_NUM_THREADS=" );
  strcat (prepends, p );
  strcat (prepends, ";" );
}

void
process_profile_modifier(char *verb, char* mod, bool is_event)
{
  /* process a modifier */
  if (mod != NULL) {
    switch (mod[0] ) {
      case 'h':
        strcat (runargs, "@f3000");
        break;
      case 'H':
	if ( is_event == true ) { 
	  // event modifiers can't be as much as CPUTIME or REALTIME
          strcat (runargs, "@f12500");
	} else {
	  strcat (runargs, "@f75000");
	}
        break;

      case 'L':
      case 'l':
        strcat (runargs, "@f30");
        break;
      default:
        sprintf(err_buf, "invalid modifier for %s \"%s\"", verb, mod);
        err_exit ( err_buf);
    }
  }
}

void
process_lvl0_modifier(char *verb, char* mod)
{
  bool docount = false;
  bool dosilent = false;
  bool first=true;
  bool done=false;

  /* process a modifier */
  if (mod != NULL) {
    if ( strcmp(mod, "pc" ) == 0 ) {
      strcat (runargs, ",pc");
    }
    else if ( strncmp(mod, "k=" , 2 ) == 0 ) {
      for (int i=2 ;; i++ ) {
	if (done == true ) break;  // from the loop
        switch (mod[i] ) {
        case 0 :
	  done = true;
          break;
        case 'c':
          docount = true;
          break;
        case 's':
          dosilent = true;
          break;
        default:
          sprintf(err_buf, "invalid %s modifier \"%s\"", verb, mod);
          err_exit ( err_buf);
        }
      }

      // We're done with the string, format the real modifier
      if (docount == true ) {
        if (first == true) {
           strcat (runargs, ",inst=count");
           first = false;
        } else {
           strcat (runargs, ",count");  // this can't happen; count is first */
        }
      }
      if (dosilent == true ) {
        if (first == true) {
           strcat (runargs, ",inst=silent");
           first = false;
        } else {
           strcat (runargs, ",silent");
        }
      }
    }
  } else {
    // modifier is NULL
  }
}

void
cputime(char *verb, char *mod)
{
  /* enable CPUTIME data collection */
  strcat (runargs, " -e CPUTIME" );
  process_profile_modifier(verb, mod, false);
}

void
cycles(char *verb, char *mod)
{
  /* enable cycles data collection */
  strcat (runargs, " -e cycles" );
  process_profile_modifier(verb, mod, true);
}

void
papicycles(char *verb, char *mod)
{
  /* enable cycles data collection */
  strcat (runargs, " -e PAPI_TOT_CYC" );
  if (mod != NULL ) {
    sprintf( err_buf, "%s may not have a modifier (%s)", verb, mod);
    err_exit(err_buf);
  }
}

void
realtime(char *verb, char *mod)
{
  /* enable REALTIME data collection */
  strcat (runargs, " -e REALTIME" );
  process_profile_modifier(verb, mod, false);
}

void
insts(char *verb, char *mod)
{
  /* enable instructions data collection */
  strcat (runargs, " -e instructions" );
  process_profile_modifier(verb, mod, true);
}

void
lvl0(char *verb, char *mod)
{
  /* enable intel GPU data collection */
  strcat (runargs, " -e gpu=level0" );
  process_lvl0_modifier(verb, mod);
}

void
cuda(char *, char *mod)
{
  if (mod == NULL ) {
    /* enable Nvidia GPU data collection, without PC Sampling */
    strcat (runargs, " -e gpu=cuda" );
    return;
  }
  if (strcmp (mod, "pc" ) == 0 ) {
    /* enable Nvidia GPU data collection, with PC Sampling */
    strcat (runargs, " -e gpu=cuda,pc" );
    strcat (hpcstructargs, "--gpucfg yes" );
    return;
  }
  sprintf( err_buf, "cuda may not have a modifier of %s", mod);
  err_exit(err_buf);
}

void
rocm(char *, char *mod)
{
  if (mod == NULL ) {
    /* enable AMD GPU data collection, without PC Sampling */
    strcat (runargs, " -e gpu=rocm" );
    return;
  }
  if (strcmp (mod, "pc") == 0 ) {
    /* enable AMD GPU data collection, with PC Sampling */
    strcat (runargs, " -e gpu=rocm,pc" );
    strcat (hpcstructargs, "--gpucfg yes" );
    return;
  }
  sprintf( err_buf, "rocm may not have a modifier of %s", mod);
  err_exit(err_buf);
}

void
rocmcounter(char *verb, char *mod)
{
  if (mod != NULL ) {
    sprintf( err_buf, "%s may not have a modifier (%s)\n", verb, mod);
    err_exit(err_buf);
  }

  /* enable a rocm counter data collection */
  char thisarg[4096];
//      rocm::
//      Convert to the real counter name, since the __ name can't be used as the real counter name
  verb[4] = ':' ;
  verb[5] = ':' ;
  sprintf (thisarg, " -e %s", verb );
  strcat (runargs, thisarg );
}

void
debugon(char *, char *mod)
{
  if (mod != NULL ) {
    sprintf( err_buf, "d may not have a modifier (%s)", mod);
    err_exit(err_buf);
  }

  /* enable runtime debugging during data collection */
  strcat (runargs, " -d " );
}
void
dbldebugon(char *verb, char *mod)
{
  if ( mod == NULL ) {
    sprintf( err_buf, "dd must have a modifier" );
    err_exit(err_buf);
  }

  /* enable runtime debugging during data collection */
  strcat (runargs, " -dd " );
  strcat (runargs, mod );
}

void
traceon(char *, char *mod)
{
  if (mod != NULL ) {
    sprintf( err_buf, "may not have a modifier (%s)", mod);
    err_exit(err_buf);
  }

  /* enable tracing in the data collection */
  strcat (runargs, " -t" );
}

void
tracker(char *, char *mod)
{
  if (mod != NULL ) {
    sprintf( err_buf, "tracker may not have a modifier (%s)", mod);
    err_exit(err_buf);
  }

  /* enable tracker */
  strcat (prepends, "export RUN_TRACKER=1; " );
}

void
mpitracker(char *, char *mod)
{
  if (mod != NULL ) {
    sprintf( err_buf, "mpitracker may not have a modifier (%s)", mod);
    err_exit(err_buf);
  }

  /* enable mpitracker */
  strcat (prepends, "export RUN_MPITRACKER=1; " );
}

void
argoverride(char *, char *mod)
{
  if (mod == NULL ) {
    sprintf( err_buf, "arg must have a quoted string modifier");
    err_exit(err_buf);
  }
  //add to the usrtargargs
  strcat(usrtargargs, " ");
  strcat(usrtargargs, mod);
}

void
python(char *, char *mod)
{
  if (mod != NULL ) {
    sprintf( err_buf, "arg must have a quoted string modifier");
    err_exit(err_buf);
  }
  //add to the runargs
  strcat (runargs, " -a python" );
}

/* pass the -N argument to the minitest target */
void
minitestN(char *, char *mod)
{
  if (mod == NULL ) {
    sprintf( err_buf, "MN must have a numeric modifier" );
    err_exit(err_buf);
  }

  strcat(targargs, " -N ");
  strcat(targargs, mod );
}

/* pass the -I argument to the minitest target */
void
minitestI(char *, char *mod)
{
  if (mod == NULL ) {
    sprintf( err_buf, "MI must have a numeric modifier" );
    err_exit(err_buf);
  }

  strcat(targargs, " -I ");
  strcat(targargs, mod );
}

/* pass the -K argument to the minitest target */
void
minitestK(char *, char *mod)
{
  if (mod == NULL ) {
    sprintf( err_buf, "MK must have a numeric modifier" );
    err_exit(err_buf);
  }

  strcat(targargs, " -K ");
  strcat(targargs, mod );
}

void
mpisetup(char *, char *mod)
{
  if (mod == NULL ) {
    sprintf( err_buf, "mpi must have a numeric modifier" );
    err_exit(err_buf);
  }

  /* setup for an MPI run */
  mpimode = true;
  parse_number(mod);
  mpiranks = mod;
}

void
userhpcarg(char *verb, char *mod)
{
  strcat (runargs, " -e " );
  strcat (runargs, verb );

  if (mod != NULL ) {
    // There was a comma; restore it, and then add the modifier
    strcat (runargs, "," );
    strcat (runargs, mod );
  }
}

void
dotimer( char *value)
{
  char buf [128];
  sprintf( buf,  "export time_out=\"timeout -s6 %s \" ;  ", value );
  // fprint(fstderr, " -timeout(%s) exporting %s\n", value, buf);
  strcat (prepends, buf);
}

void
print_help(void)
{
  struct  scripttab  *k;
  printf("\n qadohpct help: qadohpct is invoked as follows\n");
  printf("  a '-h' argument will print this help message \n");
  printf("  a '-x' argument implies do not run \n");
  printf("  a '-timeout' argument will set a timeout \n");
  printf("    after any '-' arguments are the three required arguments\n");
  printf("  first argument is \"target\"\n");
  printf("  second argument is \"target-arguments\"\n");
  printf("  third argument is the data descriptor, of the form expt.dt1...dtN\n");
  printf("    the \"expt.\" preface is mandatory; the supported \"dtn\" items are:\n");
  for (k = &scripttab[0]; ; k++) {
    /* end-of table */
    if(k->name == NULL) {
      break;
    } else {
      printf("        %s\n", k->name);
    }
  }
  printf("        NN (any decimal number)\n");
  printf("        -string (any user-defined string not containing \"/\"; must come last in the descriptor)\n");
  printf("        other (any string not starting with - and not containing period, blank, or slash\n" );
  printf("                  it will be passed to hpcrun as \"-e other\" \n");
  printf("     the \"dtn\" items may appear in any order, separated by .\n");
  printf("\n");
}

// clear any leftover error file
void
err_clear()
{
  // Deleted any possible error file name
  sprintf( cmdbuf, "/bin/rm -f qadohpct.err*" );
  system(cmdbuf);
}

void
err_exit(char *msg)
{
  char namebuf[1024];

  if (norun == true) {
    fprintf(stderr ,"%s", msg);
    exit(0);
  }

#ifdef STANDALONE
  fprintf(stderr ,"%s", msg);
  exit(1);
#else
  // Create an error file to report a syntax error from qadohpct
  sprintf( namebuf, "qadohpct.err.%s", ddesc );
  FILE *errf = fopen (namebuf, "w" );
  if (errf != NULL) {
    fprintf(errf ,"%s", msg);
  } else {
    // Oh well, can't open the log file
    fprintf(stderr, "%s\nqadohpct could not create err file named %s -- %s\n", msg, namebuf, strerror(errno) );
  }
  exit(1);
#endif
}
