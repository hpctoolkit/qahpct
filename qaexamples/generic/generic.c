/*
 * SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <cmath>
#include <time.h>

void spacer(int);

int
main(int argc, char *argv[], char **envp)
{
  fprintf(stderr, "Beginning generic cpu-burning code\n");
  spacer(500);
  fprintf(stderr, "Exit generic cpu-burning code\n");
  return 0;
}

void
spacer(int timems)
{
  // burn CPU which will be visible in the traced callstacks

  int	j,k;	/* temp values for loops */
  volatile float	x;	/* temp variable for f.p. calculation */
  long long count = 0;

  for (k= 0; k < timems; k++) {
    x = 0.0;
    for(j=0; j<1000000; j++) {
      x = x + 1.0;
    }
    count++;
  }
}
