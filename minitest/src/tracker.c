/*
 * SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdio.h>

// The name of the program depends on a passed in fal
#ifdef DO_MPI
  const char *name = "mpitracker";
#else
  const char *name = "tracker";
#endif

int
main(int argc, char** argv, char** envp)
{
  //Routine to announce itself,  burn some cpu time and then print a line and then exit

  fprintf( stderr, "   %s started\n", name );

  int	j,k;	/* temp values for loops */
  volatile float	x;	/* temp variable for f.p. calculation */
  long long count = 0;

  for (k= 0; k < 100; k++) {
    x = 0.0;
    for(j=0; j<1000000; j++) {
      x = x + 1.0;
    }
    count++;
  }

  fprintf( stderr, "   %s completed -- performed %lld while-loop iterations\n\n", name, count);

  return 0;
}

