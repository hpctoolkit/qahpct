<!--
SPDX-FileCopyrightText: Contributors to the HPCToolkit Project

SPDX-License-Identifier: CC-BY-4.0
-->

This directory is NOT in the list of rcom directories, because the CrayCC compiler does not allow OpenMP calls from the worker thread.
