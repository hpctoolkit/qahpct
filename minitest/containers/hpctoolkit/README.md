<!--
SPDX-FileCopyrightText: Contributors to the HPCToolkit Project

SPDX-License-Identifier: CC-BY-4.0
-->

The containers in this directory are not generated automatically, they are instead manually
produced as needed when the base version of HPCToolkit is updated. The `make.sh` script in
this directory automates this process.

The images built are:
  - `hpctoolkit:<VERSION>-cudaX.Y`: HPCToolkit install based on CUDA Toolkit X.Y and Ubuntu 22.04.
  - `hpctoolkit:<VERSION>-rocmX.Y`: HPCToolkit install based on ROCm X.Y and Ubuntu 22.04.

See the `*.json` files in this directory for more details on each individual image.