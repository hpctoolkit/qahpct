#!/bin/bash

# SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: Apache-2.0

# This is the Spack version of HPCToolkit that will be built.
# NB: CHANGING THIS VALUE REQUIRES A MAJOR VERSION BUMP!!!
export HPCTOOLKIT_VERSION='git.1b7ed74f6b99c0bfc935fb4678837c3ac3fd511b=2023.08.0'
HPCTOOLKIT_VERSION_SLUG="2023.08.0"  # MUST change whenever HPCTOOLKIT_VERSION does


HERE="$(realpath "$(dirname "$0")")"

if [ "$1" = "--dry-run" ]; then dryrun=1; else dryrun=; fi

if ! [ "$dryrun" ] && ! buildah login --get-login registry.gitlab.com; then
    {
        echo 'Buildah does not appear to be logged into registry.gitlab.com,'
        echo 'you need to do this first for this script to push its results:'
        echo '    buildah login registry.gitlab.com'
        echo "If you don't want to push, pass --dry-run as the only argument:"
        echo "    $0 --dry-run"
    } >&2
    exit 2
fi

if ! command -v j2 > /dev/null || ! command -v jq > /dev/null; then
    {
        echo 'This script requires both j2cli and jq to be installed on the'
        echo 'base system. Both can be installed via Pip:'
        echo '    pipx install j2cli jq'
    } >&2
    exit 2
fi

# Create a temporary directory to store all our temporary bits
trap 'rm -rf "$TMP"' EXIT
TMP="$(mktemp -d)"

declare -A digests

SPINS=(
    cuda11.8.json
    cuda12.0.json
    rocm5.3.json
    rocm5.4.json
    rocm5.5.json
    rocm5.6.json
)

(cd "$HERE"/bootstrap && spack containerize --last-stage=bootstrap > Dockerfile) || exit $?
for data in "${SPINS[@]}"; do
    slug="$(jq --raw-output '.slug // halt_error' "$HERE"/"$data")" || exit $?
    platform="$(jq --raw-output '.platform // halt_error' "$HERE"/"$data")" || exit $?
    template="$(jq --raw-output '.template // halt_error' "$HERE"/"$data")" || exit $?

    # Generate the Spack environment for the temporary core image
    rm -rf "$TMP"/env
    mkdir "$TMP"/env || exit $?
    j2 -o "$TMP"/env/spack.yaml "$HERE"/templates/"$template" "$HERE"/"$data" || exit $?

    bargs=(--format=docker --platform="$platform" --layers --cache-ttl=72h)

    # If needed, generate a bootstrap image from the common configuration
    bootstrap="$(jq --raw-output '.image.bootstrap // ""' "$HERE"/"$data")" || exit $?
    if [ "$bootstrap" ]; then
        boottag="localhost/spack-bootstrap-$slug"
        buildah build --tag="$boottag" --from="$bootstrap" "${bargs[@]}" "$HERE"/bootstrap || exit $?
    fi

    # Generate the Dockerfile for the temporary core image, and build it
    (cd "$TMP"/env && spack containerize > Dockerfile) || exit $?
    coretag="localhost/hpctoolkit-spacked-$slug"
    buildah build --tag="$coretag" "${bargs[@]}" "$TMP"/env || exit $?

    # Add the final adjustments to the image for sanity's sake
    tag="registry.gitlab.com/hpctoolkit/minitest/hpctoolkit:$HPCTOOLKIT_VERSION_SLUG-$slug"
    buildah build --tag="$tag" "${bargs[@]}" --build-arg=BASE="$coretag" "$HERE" || exit $?

    # Push the image up to the registry
    if [ -z "$dryrun" ]; then
      buildah push --digestfile="$TMP"/digest "$tag" || exit $?
    else
      echo "DRYRUN-$slug" > "$TMP"/digest
    fi
    digests[$slug]="$tag@$(cat "$TMP"/digest)" || exit $?
done

echo 'Images have been built! Now update .gitlab-ci.yml with the following digests:'
for slug in "${!digests[@]}"; do
    echo "'.hpctoolkit: [$slug]':"
    echo "  variables:"
    echo "    HPCTOOLKIT_IMG: '${digests[$slug]}'"
done