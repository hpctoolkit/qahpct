#!/bin/bash

# SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: Apache-2.0

#  Script to run a build, executed in a subshell

buildall()
{
	echo "--> Building in directory $testdir" | tee -a $master_logname
	unset QA_TEST_VARIANT
	# check for and then source the appropriate setup-env file
	if [ -f setup-env/$thishost.sh ]; then
		echo "Sourcing setup-env/$thishost.sh before building" | tee -a $master_logname
		source setup-env/$thishost.sh
	else
		echo "File setup-env/$thishost.sh does not exist; ignoring setup " | tee -a $master_logname
	fi

	echo "QA_TEST_VARIANT = $QA_TEST_VARIANT" | tee -a $master_logname

	# first clean out the directory
	make veryclean | tee -a $master_logname

	# now execute the build
	make build | tee -a $master_logname
	makestatus=$?

	# and show what is now in the directory
	ls -l | tee -a $master_logname

	if [ "$makestatus" != "0" ]; then
		echo " *** BUILD FAILED -- make in ${testdir} failed; makestatus = ${makestatus} " | tee -a $master_logname
	else
		echo " *** BUILD SUCCEEDED -- make in ${testdir} succeeded" | tee -a $master_logname
	fi
}
