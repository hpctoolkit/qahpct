<!--
SPDX-FileCopyrightText: Contributors to the HPCToolkit Project

SPDX-License-Identifier: CC-BY-4.0
-->

# `QAHPCT--Tests and Data-Descriptors`

**HPCToolkit Experiments and Data-Descriptors**

The end-product of an HPCToolkit experiment is a database
directory which contains the information recorded
and processed to be visualized with `hpcviewer`.
Although primarily used for recording Experiments,
the `QAHPCT` infrastructure also supports tests that simply run
the application.

Each test is specified by a `<Data-Descriptor>`, and
describes a single execution of the target application,
with or without HPCToolkit recording.
When recording data, the `<Data-Descriptor>` is of the form:
`expt.dt1.dt2...dtM`.
When not recording data, the `<Data-Descriptor>` is of the form:
`run.dt1.dt2...dtM`.
A Data-Descriptor with any other prefix is invalid.

After the prefix, the various `dtx` fields can appear in any order.
Most of the `dtx` elements refer to a data collection argument for
`hpcrun`; such elements are inapplicable to
a test without data collection (i.e., a `run.dt1...` test).
Some of the `dtx` fields may take a modifier, as shown below.
Other `dtx`s specify an environment variable to be set before
running the data collection, or command-line arguments to the
application.

Each test will produce a logfile in the QA-Test-Directory,
named as `log.<Data-Descriptor>`,
describing how the test was run and what results were obtained.

For experiments, the first step is running
the test code under `hpcrun` and collecting a
measurements directory, named `meas.<Data-Descriptor>`.
That directory is then processed by `hpcstruct`,
to analyze the program's structure,
and then by`hpcprof` to reduce the raw data and write
the database directory, named `dbase.<Data-Descriptor>`.

The supported `dtx` types and their corresponding `hpcrun` argument
and meaning are shown in the various tables below;
where the `hpcrun` argument is shown as N/A, the `dtx` type
does not affect data collection,
and may be used in a `run.` invocation; otherwise it may
not be used in a `run.` invocation.

Data-Descriptor elements specifying data collection items;
applicable only to `expt.*` tests:

| `dtx` | `hpcrun` Argument | Meaning |
|-------|----------------------------|---------|
| `t` | `-t` | Collect trace data |
| `cputime[,M]` | `-e CPUTIME` | Collect User CPU time data; see below for `[,M]` meaning |
| `cycles[,M]` | `-e cycles` | Collect User CPU cycles data; see below for `[,M]`  meaning |
| `papicycles` | `-e PAPI_TOT_CYC` | Collect User CPU cycles with PAPI data |
| `realtime[,M]` | `-e REALTIME` | Collect real time data; see below for `[,M]` meaning  |
| `insts[,M]` | `-e instructions` | Collect Executed instruction count data; see below for `[,M]` meaning |
| `python` | `-a python` | Collect data for user-python interpreatation |
| `<other>` |`-e <other>` | `<other>`is assumed to be an `hpcrun` data option; it may not contain any spaces, periods or slashes, nor begin with a minus sign |

The optional modifiers described above as `[,M]`,
are either `,h`, for high-resolution profiling,
`,H`, for very-high-resolution
profiling, or `,l` for low-resolution profiling.


Data Descriptor arguments for GPU data-collection are:

| `dtx` | `hpcrun` Argument | Meaning |
|-------|----------------------------|---------|
| `cuda[,pc]` | `-e gpu=nvidia[,pc]` | Collect Nvidia GPU metric data; if `,pc` is specified,  also collect GPU PC sampling data; invoke `hpcstruct` with the `--gpucfg yes` argument |
| `rocm[,pc]` | Collect ROCM GPU  metric data; if `,pc`is specified,  also collect GPU PC sampling data |
| `rocm__<name>` | `-e rocm::<name>` | Collect AMD rocm data for counter `<name>` |
| `lvl0[,T]` | `-e gpu=level0[,X]` | Collect Intel GPU metric data; see below for `[,T]` meaning and its mapping to `[,X]` |

For the `lvl0` element, the modifier is described as `[,T]`.
`,T` can be `,pc` in which case `,pc` is appended to the `lvl0` command.
`,T` can also be `,k=` followed by a one or two letter
combination of `c` for `count`; or `s` for `silent`.
If specified, an additional parameter `,inst=` with the types given
above is appended to the profile command.  Both options can be given.

A Data-Descriptor element of the form `rocm__<name>` will
profile the AMD GPU counter named `rocm::<name>`.

Some data-Descriptor elements specify the target's behavior;
they are applicable to both `run.*` and `expt.*` tests:

| `dtx` | `hpcrun` Argument | Meaning |
|-------|----------------------------|---------|
| `arg,%xxx%` | N/A | Replace the target argument with `xxx` |
| `_NN_` | N/A | `_NN_` is a decimal number, specifying the number of threads exported as OMP_NUM_THREADS |
| `mpi,_nnn_` | N/A | Run under MPI, with `_nnn_` ranks |
|  `-<label>` | N/A | append the `-<label>` string to all names; it`must be the last element in the descriptor |

For the `arg` element, the modifier is described as `%xxx%`;
the `%` bracketing the `xxx` string is required to allow `xxx`
to contain periods.
Multiple `arg` elements will be contatenated with a blank between them.
The `xxx` string can not contain any blanks.
To have a blank in the user argument override, replace the desired
blank by `@.arg,@`; that is, concatenate two such arguments.

Some Data-Descriptor elements are specific to the behavior of
`minitest` targets; they are applicable to both `run.*`
and `expt.*` tests:

| `dtx` | `hpcrun` Argument | Meaning |
|-------|----------------------------|---------|
| `MN,_nnn_` | N/A | Pass the `-N _nnn_` argument to the `minitest` application, specifying the array size |
| `MK,_nnn_` | N/A | Pass the `-K _nnn_` argument to the `minitest` application, specifying the kernel scale |
| `MI,_nnn_` | N/A | Pass the `-I _nnn_` argument to the `minitest` application, specifying the iteration count |
| `tracker` |  N/A | Invoke a stand-in for LLNL's `tracker` -- export RUN_TRACKER=1, as invoked in many of their applications |
| `mpitracker` |  N/A | Invoke a different stand-in for LLNL's `tracker` -- export RUN_MPITRACKER=1, as invoked in some MPI applications |

Some data-Descriptor elements are intended for developers of HPCToolkit;
they are applicable only to `expt.` tests:

| `dtx` | `hpcrun` Argument | Meaning |
|-------|----------------------------|---------|
| `d` | `-d` | Enable debugging the `hpcrun` data collection |
| `dd,<subsystem>` | `-dd <subsystem>` | Report more debugging output from a `<subsystem>` of the `hpcrun` data collection |


Here are some examples of Data-Descriptors, and their meaning:
| `Data-Descriptor` | Meaning |
|-------|-------------------------------------|
| expt.1.cputime | Set OMP_NUM_THREADS to 1; collect CPUTIME data, without trace data. |
| expt.1.realtime,h.cycles.insts.t | Set OMP_NUM_THREADS to 1; collect high-resolution REALTIME data, and perf-events cycles- and insts-data; collect trace data. |
| expt.4.cputime.lvl0,k=c.t | Set OMP_NUM_THREADS to 4; collect CPUTIME data with four threads; collect level0 count data, with tracing. |
| expt.1.cputime.cuda,pc.t | Set OMP_NUM_THREADS to 1; collect CPUTIME data, and and Nvidia GPU PC-sampling data; collect trace data. |
| expt.2.cputime.cuda.t | Set OMP_NUM_THREADS to 1; collect CPUTIME data and Nvidia GPU data; collect trace data. |
| expt.1.realtime.rocm.t | Set OMP_NUM_THREADS to 1; collect REALTIME data, and AMD GPU data; collect trace data. |
| expt.1.realtime.rocm,pc.t | Set OMP_NUM_THREADS to 1; collect REALTIME data, and AMD GPU data with PC sampling; collect trace data. |
| run.2.tracker | Set OMP_NUM_THREADS to 2; run the  `minitest` target without collecting any data, and invoke tracker before and after the body of the target. |
| expt.2.MN,30000000.MI,10.cputime.t | Set OMP_NUM_THREADS to 2; run the `minitest`target, setting the array size to 30000000 and the iteration count to 10; collect CPUTIME data and traces.
| expt.mpi,2.1.cputime.t | Run with two MPI ranks; set OMP_NUM_THREADS to 1; collect CPUTIME data, with trace data. |

